package tool

import (
	"time"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

var Mdb = new(sql.DB)

func init() {
	var err error
	dsn := "dev:CCjd1815@tcp(192.168.0.130:3306)/gin?charset=utf8mb4&parseTime=True&loc=Local"
	Mdb, err = sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}

	Mdb.SetMaxIdleConns(10)
	Mdb.SetMaxOpenConns(100)
	Mdb.SetConnMaxLifetime(time.Hour)
}
