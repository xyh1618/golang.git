package tool

import (
	"crypto/md5"
	"fmt"
)

func Md5TransString(secret string) string {
	m := md5.Sum([]byte(secret))
	mm := fmt.Sprintf("%x", m)
	return mm
}
