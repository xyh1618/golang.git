package tool

import (
	"fmt"

	"os/exec"
	"strconv"
	"strings"
)

// 程序执行函数
func Implement(err error, name string) error {
	if err != nil {

		return err
	}
	menu := SelectMune(name)
	err = exec.Command("/bin/sh", "-c", fmt.Sprintf("mv -f ./process/%s /home/%s", name, menu)).Run()
	if err != nil {

		return err
	}

	err = exec.Command("/bin/sh", "-c", fmt.Sprintf("cd /home/%s && sh startup.sh", menu)).Run()

	if err != nil {

		return err
	}

	return nil

}

// 程序执行函数
func Implement1(err error, name string) error {
	if err != nil {

		return err
	}
	menu := SelectMune(name)

	err = exec.Command("/bin/sh", "-c", fmt.Sprintf("cd /home/%s && sh startup.sh", menu)).Run()

	if err != nil {

		return err
	}

	return nil

}

// 程序杀死kill执行函数
func Kill(name string) error {

	out, err := exec.Command("/bin/sh", "-c", fmt.Sprintf("ps -ef|grep -v grep|grep %s ", name)).Output()
	if err != nil {
		//有错误说明out为空内容，也就是jar程序没有启动，所以返回nil继续执行
		return nil
	}

	outSlice := strings.Fields(string(out))
	pid, _ := strconv.Atoi(outSlice[1])

	err = exec.Command("/bin/sh", "-c", fmt.Sprintf("kill -9 %d", pid)).Run()

	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// 执行目录的选择
func SelectMune(name string) string {

	switch name {
	case "rest-app.jar":
		return "wechat"
	case "rest.jar":
		return "cms"
	case "agent.jar":
		return "agent"

	case "consumer-cushion.jar":
		return "consumer"

	case "netty-new.jar":
		return "netty9999"

	case "netty-new-json.jar":
		return "netty9998"
	case "process-handler.jar":

		return "process"
	case "process-handler-json.jar":
		return "processJson"
	case "timedtask.jar":
		return "timer"
	case "wechatTimedtask.jar":
		return "wechatTime"

	default:
		return ""
	}

}
