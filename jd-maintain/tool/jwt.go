package tool

import (
	"context"

	"time"

	"github.com/golang-jwt/jwt/v4"
)

var ctx = context.Background()

type MyClaims struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
	jwt.RegisteredClaims
}

func CreateJwt(claims *MyClaims) string {
	//过期时间
	claims.ExpiresAt = jwt.NewNumericDate(time.Now().Add(time.Hour * 10))

	//签发着
	claims.Issuer = "jdyichong"
	//签名主题
	claims.Subject = "userToken"
	//当前时间
	claims.IssuedAt = jwt.NewNumericDate(time.Now())

	//redis获得密钥
	secret, err := Rdb.Get(ctx, "mySigningKey").Result()
	if err != nil {
		panic(err)
	}

	secretbyte := []byte(secret)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	str, err := token.SignedString(secretbyte)
	if err != nil {
		panic(err)
	}
	return str

}
func CheckToken(token string) (*jwt.Token, error) {
	secret, err := Rdb.Get(ctx, "mySigningKey").Result()
	if err != nil {
		panic(err)
	}

	secretbyte := []byte(secret)
	return jwt.ParseWithClaims(token, &MyClaims{}, func(tk *jwt.Token) (any, error) { return secretbyte, nil })

}
