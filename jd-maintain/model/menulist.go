package model

import (
	"jd-maintain/tool"
)

func GetMenuList(id int) any {
	menuListId := 0
	err := tool.Mdb.QueryRow("select menulist from user where id = ? ", id).Scan(&menuListId)
	tool.Checkout(err)
	json := ""
	err = tool.Mdb.QueryRow("select children from menulist where id = ? ", menuListId).Scan(&json)
	tool.Checkout(err)

	return json
}
