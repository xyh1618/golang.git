package model

import (
	"context"
	"fmt"
	"image/color"
	"jd-maintain/tool"
	"strings"
	"time"

	"github.com/mojocn/base64Captcha"
)

// var c = context.Background()
// var d = base64Captcha.DefaultMemStore

// const CAPTCHA = "captcha:"

type RedisStore struct{}

func (r *RedisStore) Set(id, value string) error {
	c := context.Background()
	key := "captcha:" + id
	err := tool.Rdb.Set(c, key, value, time.Second*60).Err()
	return err

}
func (r *RedisStore) Get(id string, clear bool) string {
	c := context.Background()
	key := "captcha:" + id
	val, err := tool.Rdb.Get(c, key).Result()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	if clear {
		err := tool.Rdb.Del(c, key).Err()
		if err != nil {
			fmt.Println(err.Error())
			return ""
		}
	}
	return val
}
func (c *RedisStore) Verify(id, answer string, clear bool) bool {
	vv := c.Get(id, clear)
	vv = strings.TrimSpace(vv)
	return vv == answer
}
func GenerateCaptcha() (string, string, error) {
	driveString := base64Captcha.DriverString{
		Height:          33,
		Width:           100,
		NoiseCount:      0,
		ShowLineOptions: 2 | 4,
		Length:          4,
		Source:          "1234567890abcdefghijklmnopgrstuvwxyz",
		BgColor:         &color.RGBA{R: 3, G: 102, B: 214, A: 125},
		Fonts:           []string{"wqy-microhei.ttc"},
	}
	drive := driveString.ConvertFonts()
	store := new(RedisStore)
	captcha := base64Captcha.NewCaptcha(drive, store)
	return captcha.Generate()

}
