package model

type Response[T SandI, U any] struct {
	Code T      `json:"code"`
	Msg  string `json:"msg"`
	Data U      `json:"data"`
}
