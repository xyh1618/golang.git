package model

import (
	"fmt"
	"jd-maintain/tool"
)

type ResetPassword struct {
	NewPassword string `json:"newpassword"`
	RePassword  string `json:"repassword"`
}

func RePassword(p string, id int) error {
	sqltx, _ := tool.Mdb.Begin()
	_, err := sqltx.Exec("update user set password=? , updatetime=? where id = ?", p, tool.CurTimeStr(), id)
	if err != nil {
		sqltx.Rollback()
		fmt.Println(err)
		return err
	} else {
		sqltx.Commit()
		return nil
	}

}
