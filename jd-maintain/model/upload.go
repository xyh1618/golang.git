package model

import (
	"jd-maintain/tool"
)

type UpdateInfo struct {
	Package    string `json:"package"`
	User       string `json:"user"`
	Updatetime string `json:"updatetime"`
	Result     string `json:"result"`
}

func Insertupdateinfo(info *UpdateInfo) error {
	_, err := tool.Mdb.Exec("insert into updateinfo (package,user,updatetime,result) values (?,?,?,?)", info.Package, info.User, info.Updatetime, info.Result)
	if err != nil {
		return err
	}
	return nil
}
