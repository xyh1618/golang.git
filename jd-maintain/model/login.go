package model

import (
	"jd-maintain/tool"
)

type LoginRequest struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Id       string `json:"id"`
	Code     string `json:"code"`
}
type LoginRespone struct {
	Id         int    `json:"id"`
	Token      string `json:"token"`
	Code       string `json:"dode"`
	ExpireTime int    `json:"expiretime"`
}

type ImgResponse[T SandI] struct {
	Id     T      `json:"id"`
	ImgSrc string `json:"imgsrc"`
}

func GetUser(name string) (i, u, p string) {
	id := ""
	username := ""
	password := ""
	tool.Mdb.QueryRow("select id,username,password from user where username = ?", name).Scan(&id, &username, &password)

	return id, username, password
}
