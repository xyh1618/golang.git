package model

import (
	"jd-maintain/tool"
	"strconv"
)

type RecordList struct {
	Lists []*UpdateInfo `json:"lists"`
	Total int           `json:"total"`
}

func OperateSql(n, p, c string) (*RecordList, error) {
	pint, _ := strconv.Atoi(p)
	cint, _ := strconv.Atoi(c)
	record := new(RecordList)

	total := 0
	package1 := ""
	user1 := ""
	updatetime1 := ""
	result1 := ""
	err := tool.Mdb.QueryRow("select count(*) from updateinfo").Scan(&total)
	if err != nil {

		return record, err
	}
	record.Total = total
	record.Lists = make([]*UpdateInfo, 0)
	if n == "" {
		rows, err := tool.Mdb.Query("select package, user, updatetime ,result from updateinfo order by id desc limit ?,? ", cint-1, pint)
		defer rows.Close()
		if err != nil {

			return record, err
		}

		for rows.Next() {
			err = rows.Scan(&package1, &user1, &updatetime1, &result1)
			if err != nil {

				return record, err
			}
			list := new(UpdateInfo)
			list.Package = package1

			list.User = user1
			list.Updatetime = updatetime1
			list.Result = result1

			record.Lists = append(record.Lists, list)

		}
		return record, nil
	} else {
		rows, err := tool.Mdb.Query("select package, user, updatetime ,result from updateinfo where user=? order by id desc limit ?,? ", n, cint-1, pint)
		defer rows.Close()
		if err != nil {

			return record, err
		}

		for rows.Next() {
			err = rows.Scan(&package1, &user1, &updatetime1, &result1)
			if err != nil {

				return record, err
			}
			list := new(UpdateInfo)
			list.Package = package1
			list.User = user1
			list.Updatetime = updatetime1
			list.Result = result1
			record.Lists = append(record.Lists, list)
		}
		return record, nil

	}

}
