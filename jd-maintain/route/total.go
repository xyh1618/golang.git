package route

import (
	"jd-maintain/controller"
	"jd-maintain/middleware"
	"jd-maintain/tool"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var r = gin.Default()

func init() {
	//设置代理信任ip，默认信任所有代理ip
	// r.SetTrustedProxies([]string{"127.0.0.1"})
	r.Use(cors.New(middleware.Config)).Use(middleware.Ip())
}
func App() {
	r.LoadHTMLFiles("index.tmpl")

	r.GET("/login/img", controller.CaptchaApi)

	r.POST("/login", controller.LoginApi)
	r.GET("/hello", controller.HelloApi)
	r.GET("/create", controller.CreateApi) //程序测试专用接口http://localhost:8090/create
	api := r.Group("/api", middleware.Jwts())
	{
		api.GET("/menu/getMenuList", controller.MenuListApi)
		api.GET("/hello", controller.HelloApi)
		api.POST("/upload", controller.UploadFile)
		api.GET("/operate/list", controller.OperateInfo)
		api.POST("/restPass", controller.RestPass)
		api.GET("/process/reset", controller.ResetProcess)
		api.GET("/log/logserver", controller.LogServer)
	}

	//启动路由
	err := r.Run(":8080")
	tool.Checkout(err)

}
