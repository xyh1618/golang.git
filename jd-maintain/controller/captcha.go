package controller

import (
	"jd-maintain/model"

	"github.com/gin-gonic/gin"
)

func CaptchaApi(c *gin.Context) {
	id, image, _ := model.GenerateCaptcha()

	data := new(model.ImgResponse[string])
	data.Id = id
	data.ImgSrc = image
	c.JSON(200, gin.H{"code": 200, "msg": "图片发送成功", "data": data})
}
