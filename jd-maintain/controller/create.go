package controller

import (
	"encoding/json"

	"jd-maintain/tool"

	"github.com/gin-gonic/gin"
)

func CreateApi(c *gin.Context) {
	//系统

	meta0 := map[string]any{"title": "系统列表", "icon": "Setting"}
	meta1 := map[string]any{"title": "升级管理", "icon": "Sort"}
	meta2 := map[string]any{"title": "降级管理", "icon": "Sort"}
	meta3 := map[string]any{"title": "服务器管理", "icon": "Coin"}

	children0 := map[string]any{"path": "/update", "component": "/sys/update/update", "name": "update", "meta": meta1}
	children1 := map[string]any{"path": "/downdata", "component": "/sys/downdata/downdata", "name": "downdata", "meta": meta2}
	children2 := map[string]any{"path": "/server", "component": "/sys/server/server", "name": "server", "meta": meta3}
	sys := map[string]any{"path": "/sysList", "component": "Layout", "name": "sysList", "meta": meta0, "children": []any{children0, children1, children2}}

	//用户
	usermeta0 := map[string]any{"title": "用户列表", "icon": "Avatar"}
	usermeta1 := map[string]any{"title": "用户管理", "icon": "UserFilled"}
	usermeta2 := map[string]any{"title": "权限管理", "icon": "Stamp"}
	userchildren1 := map[string]any{"path": "/manager", "component": "/user/manager/manager", "name": "manager", "meta": usermeta1}
	userchildren2 := map[string]any{"path": "/auth", "component": "/user/auth/auth", "name": "auth", "meta": usermeta2}

	user := map[string]any{"path": "/userList", "component": "Layout", "name": "userList", "meta": usermeta0, "children": []any{userchildren1, userchildren2}}
	//日志管理
	logmeta0 := map[string]any{"title": "日志列表", "icon": "View"}
	logmeta1 := map[string]any{"title": "代理商", "icon": "ChatRound"}
	logmeta2 := map[string]any{"title": "后台", "icon": "ChatSquare"}
	logmeta3 := map[string]any{"title": "微信", "icon": "ChatDotSquare"}
	logmeta4 := map[string]any{"title": "consumer", "icon": "ChatLineSquare"}
	logmeta5 := map[string]any{"title": "消费程序", "icon": "Crop"}
	logmeta6 := map[string]any{"title": "二合一消费", "icon": "CreditCard"}
	logmeta7 := map[string]any{"title": "微信定时器", "icon": "Connection"}
	logmeta8 := map[string]any{"title": "定时器", "icon": "Compass"}
	logmeta9 := map[string]any{"title": "netty9999", "icon": "Van"}
	logmeta10 := map[string]any{"title": "netty9998", "icon": "Guide"}
	logchildren1 := map[string]any{"path": "/agent", "component": "/log/agent", "name": "agent", "meta": logmeta1}
	logchildren2 := map[string]any{"path": "/cms", "component": "/log/cms", "name": "cms", "meta": logmeta2}
	logchildren3 := map[string]any{"path": "/wechat", "component": "/log/wechat", "name": "wechat", "meta": logmeta3}
	logchildren4 := map[string]any{"path": "/consumer", "component": "/log/consumer", "name": "consumer", "meta": logmeta4}
	logchildren5 := map[string]any{"path": "/process", "component": "/log/process", "name": "process", "meta": logmeta5}
	logchildren6 := map[string]any{"path": "/processJson", "component": "/log/processJson", "name": "processJson", "meta": logmeta6}
	logchildren7 := map[string]any{"path": "/wechatTimer", "component": "/log/wechatTimer", "name": "wechatTimer", "meta": logmeta7}
	logchildren8 := map[string]any{"path": "/timer", "component": "/log/timer", "name": "timer", "meta": logmeta8}
	logchildren9 := map[string]any{"path": "/netty9999", "component": "/log/netty9999", "name": "netty9999", "meta": logmeta9}
	logchildren10 := map[string]any{"path": "/netty9998", "component": "/log/netty9998", "name": "netty9998", "meta": logmeta10}

	log := map[string]any{"path": "/logList", "component": "Layout", "name": "logList", "meta": logmeta0, "children": []any{logchildren1, logchildren2, logchildren3, logchildren4, logchildren5, logchildren6, logchildren7, logchildren8, logchildren9, logchildren10}}
	//其他
	othermeta0 := map[string]any{"title": "其他列表", "icon": "Files"}
	othermeta1 := map[string]any{"title": "操作记录", "icon": "Film"}

	otherchildren1 := map[string]any{"path": "/handle", "component": "/other/handle", "name": "handle", "meta": othermeta1}

	other := map[string]any{"path": "/otherList", "component": "Layout", "name": "otherList", "meta": othermeta0, "children": []any{otherchildren1}}

	total := []any{sys, user, log, other}
	js, err := json.Marshal(total)
	tool.Checkout(err)
	sql, err := tool.Mdb.Prepare("update menulist set children = ? where id = 1 ")
	tool.Checkout(err)
	_, err = sql.Exec(string(js))
	tool.Checkout(err)

	c.JSON(200, gin.H{"msg": "恢复成功", "code": 200, "data": total})
}
