package controller

import (
	"crypto/md5"
	"fmt"
	"jd-maintain/model"

	"strconv"

	"github.com/gin-gonic/gin"
)

func RestPass(c *gin.Context) {
	id, _ := c.Get("id")
	idint, _ := strconv.Atoi(id.(string))

	json := new(model.ResetPassword)
	c.BindJSON(json)
	data := md5.Sum([]byte(json.NewPassword))

	data16 := fmt.Sprintf("%x", data)

	err := model.RePassword(data16, idint)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{"code": 201, "msg": "密码更新失败"})
		return
	}

	c.JSON(200, gin.H{"code": 200, "msg": "密码更新成功"})
}
