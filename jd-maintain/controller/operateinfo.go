package controller

import (
	"jd-maintain/model"

	"github.com/gin-gonic/gin"
)

func OperateInfo(c *gin.Context) {
	pageSize := c.DefaultQuery("pageSize", "10")
	currentPage := c.DefaultQuery("currentPage", "1")
	name := c.DefaultQuery("name", "")

	record, err := model.OperateSql(name, pageSize, currentPage)

	if err != nil {

		c.JSON(200, gin.H{"code": 200, "msg": "失败"})
		return
	}
	c.JSON(200, gin.H{"code": 200, "msg": "成功", "data": record})
}
