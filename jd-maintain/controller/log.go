package controller

import (
	"fmt"
	"jd-maintain/model"
	"jd-maintain/tool"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/hpcloud/tail"
)

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}
var config = tail.Config{
	ReOpen:    true,
	MustExist: false,
	Poll:      true,
	Follow:    true,
	Location:  &tail.SeekInfo{Offset: 0, Whence: 2},
}

func LogServer(c *gin.Context) {

	val := c.Query("name")
	valName := tool.SelectMune(val)
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)

	if err != nil {
		fmt.Println(err)
		return
	}
	defer ws.Close()
	for {
		mt, msg, err := ws.ReadMessage()
		if err != nil {

			break
		}

		if string(msg) == "stop" {
			err = ws.WriteMessage(mt, []byte("stop .... ok"))

			break

		} else if string(msg) == "start" {

			go longTcp(ws, mt, valName)
		} else {
			err = ws.WriteMessage(mt, []byte("start ... ok"))
			if err != nil {
				break
			}
		}

	}
}
func longTcp(ws *websocket.Conn, mt int, name string) {
	tails, err := tail.TailFile(fmt.Sprintf("/home/%s/nohup.out", name), config)
	if err != nil {

		fmt.Println(err)
	}

	for {
		line, ok := <-tails.Lines
		fmt.Println(line.Text)
		if !ok {
			fmt.Println("tail file close")
			time.Sleep(time.Second * 1)
			continue
		}

		err = ws.WriteMessage(mt, []byte(line.Text))
		if err != nil {
			break
		}
	}
}
func ResetProcess(c *gin.Context) {

	val := c.Query("name")
	resetInfo := new(model.UpdateInfo)
	vl, _ := c.Get("user")
	err := tool.Kill(val)

	time.Sleep(time.Second * 5)
	err = tool.Implement1(err, val)
	resetInfo.Package = val
	resetInfo.User = vl.(string)
	resetInfo.Updatetime = tool.CurTimeStr()

	if err != nil {
		resetInfo.Result = "重启失败"
		err = model.Insertupdateinfo(resetInfo)
		if err != nil {
			fmt.Println(err)
		}
		c.JSON(200, gin.H{"code": 201, "msg": "重启失败"})
		return
	}
	resetInfo.Result = "重启成功"
	err = model.Insertupdateinfo(resetInfo)
	if err != nil {
		fmt.Println(err)
	}
	c.JSON(200, gin.H{"code": 200, "msg": "重启成功"})
}
