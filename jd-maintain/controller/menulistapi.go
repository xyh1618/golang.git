package controller

import (
	"fmt"
	"jd-maintain/model"
	"strconv"

	"github.com/gin-gonic/gin"
)

func MenuListApi(c *gin.Context) {
	__id, _ := c.Get("id")
	_id := __id.(string)
	id, _ := strconv.Atoi(_id)

	json := model.GetMenuList(id)
	fmt.Println(json, 999)
	c.JSON(200, gin.H{"code": 200, "msg": "success", "data": json})
}
