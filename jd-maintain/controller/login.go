package controller

import (
	"encoding/json"

	"jd-maintain/model"
	"jd-maintain/tool"

	"time"

	"github.com/gin-gonic/gin"
)

type User struct {
	ID        int       `json:"id" db:"id"`
	UserName  string    `json:"username" db:"username"`
	Password  string    `json:"password" db:"password"`
	TokenHash string    `json:"takenhash" db:"tokenhash"`
	CreatedAt time.Time `json:"createdat" db:"createdat"`
	UpdateAt  time.Time `json:"updateat" db:"updateat"`
}

// var c = context.Background()

func LoginApi(ctx *gin.Context) {
	//实例化请求参数
	req := new(model.LoginRequest)
	//获取请求JSON并转换为实例
	json.NewDecoder(ctx.Request.Body).Decode(req)
	//验证用户名
	i, u, p := model.GetUser(req.UserName)

	if u == "" {
		ctx.JSON(200, gin.H{"code": 201, "msg": "用户不存在"})
		return

	}
	//验证密码
	if p != req.Password {
		ctx.JSON(200, gin.H{"code": 202, "msg": "密码错误"})
		return

	}
	//验证验证码
	redisStore := new(model.RedisStore)
	boo := redisStore.Verify(req.Id, req.Code, true)
	if !boo {
		ctx.JSON(200, gin.H{"code": 203, "msg": "验证码错误"})
		return
	}

	myclaims := new(tool.MyClaims)
	myclaims.Id = i
	myclaims.Name = u
	myclaims.Password = p

	str := tool.CreateJwt(myclaims)

	make := map[string]any{"id": myclaims.Id, "token": str, "expireTime": myclaims.ExpiresAt}
	ctx.JSON(200, gin.H{"code": 200, "msg": "登陆成功", "data": make})
}
