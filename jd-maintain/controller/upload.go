package controller

import (
	"fmt"
	"io/ioutil"
	"jd-maintain/model"
	"jd-maintain/tool"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

func UploadFile(c *gin.Context) {
	updateinfo := new(model.UpdateInfo)

	vl, _ := c.Get("user")

	file, err := c.FormFile("file")

	tool.Checkout(err)
	processFiles, err := ioutil.ReadDir("./process")
	tool.Checkout(err)
	if len(processFiles) > 0 {
		for _, processfile := range processFiles {
			if processfile.Name() == file.Filename {
				err = os.Remove("./process/" + processfile.Name())
				tool.Checkout(err)
			}
		}
	}

	err = c.SaveUploadedFile(file, "./process/"+file.Filename)
	tool.Checkout(err)
	err = tool.Kill(file.Filename)
	time.Sleep(time.Second * 5)
	err = tool.Implement(err, file.Filename)
	updateinfo.Package = file.Filename
	updateinfo.User = vl.(string)

	updateinfo.Updatetime = tool.CurTimeStr()
	if err != nil {
		updateinfo.Result = "更新失败"
		err = model.Insertupdateinfo(updateinfo)
		if err != nil {
			fmt.Println(err)
		}
		c.JSON(200, gin.H{"code": 201, "msg": "更新失败"})
		return
	}
	updateinfo.Result = "更新成功"
	err = model.Insertupdateinfo(updateinfo)
	if err != nil {
		fmt.Println(err)
	}
	c.JSON(200, gin.H{"code": 200, "msg": "更新成功"})

}
