package middleware

import (
	// "fmt"

	"time"

	"github.com/gin-contrib/cors"
	// "github.com/gin-gonic/gin"
	// "net/http"
)

// 跨域解决中间件二选一
var Config = cors.Config{}

func init() {

	Config.AllowAllOrigins = true
	// Config.AllowOrigins = []string{"http://localhost", "http://127.0.0.1"}
	Config.AllowMethods = []string{"POST", "GET", "PUT", "DELETE"}
	Config.AllowWebSockets = true
	Config.AllowHeaders = []string{"Content-Type", "AccessToken", "X-CSRF-Token", "Authorization", "Token"}
	Config.ExposeHeaders = []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"}
	Config.AllowCredentials = true
	Config.MaxAge = 10 * time.Hour
}

// 跨域解决中间件
// func Cors() gin.HandlerFunc {
// 	return func(context *gin.Context) {
// 		method := context.Request.Method
// 		context.Header("Access-Control-Allow-Origin", "*")
// 		context.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
// 		context.Header("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE")
// 		context.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
// 		context.Header("Access-Control-Allow-Credentials", "true")
// 		if method == "OPTIONS" {
// 			context.AbortWithStatus(http.StatusNoContent)
// 		}
// 		context.Next()

//		}
//	}
