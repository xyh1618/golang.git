package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func Ip() gin.HandlerFunc {
	return func(c *gin.Context) {

		//获取客户端ip
		cIp := c.ClientIP()
		fmt.Println(cIp)
		//获取代理ip
		rIp := c.RemoteIP()
		fmt.Println(rIp)

		//完成跳转
		c.Next()

	}
}
