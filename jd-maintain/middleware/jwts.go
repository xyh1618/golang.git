package middleware

import (
	"fmt"
	"jd-maintain/tool"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Jwts() gin.HandlerFunc {
	return func(c *gin.Context) {

		if c.GetHeader("Connection") == "Upgrade" || c.GetHeader("Connection") == "upgrade" {
			tokenQuery := c.Query("token")
			fmt.Println(c.GetHeader("Connection"), 3333333333)
			token, err := tool.CheckToken(tokenQuery)
			claims, ok := token.Claims.(*tool.MyClaims)
			if err != nil || !ok {
				c.JSON(http.StatusOK, gin.H{"code": 205, "msg": err.Error()})
				c.Abort()
				return
			}
			c.Set("id", claims.Id)
			c.Set("user", claims.Name)
			c.Next()
		} else {
			tokenStr := c.GetHeader("token")

			if tokenStr == "" {

				c.JSON(http.StatusOK, gin.H{"code": 204, "msg": "用户不存在"})
				c.Abort()
				return
			}

			token, err := tool.CheckToken(tokenStr)
			claims, ok := token.Claims.(*tool.MyClaims)
			if err != nil || !ok {
				c.JSON(http.StatusOK, gin.H{"code": 205, "msg": err.Error()})
				c.Abort()
				return
			}
			c.Set("id", claims.Id)
			c.Set("user", claims.Name)
			c.Next()

		}
	}
}
