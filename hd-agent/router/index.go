package router

import (
	"agent/controller"
	"agent/middleware"
	"agent/utils"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var r = gin.Default()

func init() {
	r.Use(cors.New(middleware.Config))
	// r.Use(cors.New(middleware.CreateCros())).Use(middleware.Ip())
	// r.Use(middleware.Cors()).Use(middleware.Ip())
}
func App() {
	//创建list

	r.GET("/logInfo/:value", controller.LogServer)
	r.GET("/logError/:value", controller.LogServer)
	r.GET("/searchDir/:value", controller.SearchDir)
	r.GET("/deleteFile/:value", controller.DeleteFile)
	//启动路由
	err := r.Run(":6666")
	utils.Checkout(err)
}
