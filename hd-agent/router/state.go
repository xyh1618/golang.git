package router

import (
	"agent/utils"
	"context"
	"encoding/json"
	"fmt"
	"net"

	"strings"

	"time"

	"github.com/shirou/gopsutil/v3/cpu"

	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/mem"
)

func ComputerData() {

	//定义上下文
	ctx_llen, cancel_llen := context.WithCancel(context.Background())

	ctx_lpop, cancel_lpop := context.WithCancel(context.Background())

	ctx_rpush, cancel_rpush := context.WithCancel(context.Background())

	defer func() {
		cancel_llen()

		cancel_lpop()

		cancel_rpush()

	}()

	//获得服务器ip
	strIp := ""
	ips, _ := net.InterfaceAddrs()

	for _, ip := range ips {

		if strings.HasPrefix(ip.String(), "192") || strings.HasPrefix(ip.String(), "10") {

			strIp = ip.String()

		}
	}
	//定义数据的结构体
	computer := new(SData)
	//获得当前的时间戳
	// timer := time.Now().Unix()
	timer := time.Now().Format("01/02 15:04")
	_timer := strings.Split(timer, " ")
	computer.DateTime.Date = _timer[0]
	computer.DateTime.Time = _timer[1]
	//内存管理总量和可使用量
	v, _ := mem.VirtualMemory()
	computer.Mem.MemTotal = fmt.Sprintf("%.3f", float64(v.Total)/1024/1024/1024)
	computer.Mem.MemAvailable = fmt.Sprintf("%.3f", float64(v.Available)/1024/1024/1024)
	// fmt.Println(v.Total/1024/1024, v.Available/1024/1024, v.Used/1024/1024, v.Free/1024/1024/1024, float64(v.Free)/1024/1024/1024)
	// fmt.Println(fmt.Sprintf("%.2fg", float64(v.Free)/1024/1024/1024), 11111)

	//cpu使用率
	// a, _ := cpu.Info()
	c1, _ := cpu.Percent(time.Second, false)
	// fmt.Println(fmt.Sprintf("%.2f", c1[0]), a[0].Cores, 22222)
	// fmt.Println(c1, 9876)

	computer.Cpu.CpuUsed = fmt.Sprintf("%.2f", c1[0])
	//硬盘总量和已使用
	d, _ := disk.Usage("/")
	// fmt.Println(d.Total/1024/1024/1024, d.Used/1024/1024/1024, d.UsedPercent/1024/1024/1024, 33333)
	computer.Disk.DiskTotal = fmt.Sprint(d.Total / 1024 / 1024 / 1024)
	computer.Disk.DiskUsed = fmt.Sprint(d.Used / 1024 / 1024 / 1024)

	js, _ := json.Marshal(computer)
	// //统计一个小时的list
	// count1, err := utils.Rdb.LLen(ctx_llen1, fmt.Sprintf("computerlist:hour:%s", strIp)).Result()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// //统计一天的list
	// count2, err := utils.Rdb.LLen(ctx_llen2, fmt.Sprintf("computerlist:day:%s", strIp)).Result()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	//统计7天的list
	count, err := utils.Rdb.LLen(ctx_llen, fmt.Sprintf("computerlist:%s", strIp)).Result()
	if err != nil {
		fmt.Println(err)
	}
	//redis存入数据，list保持一个小时
	// go func() {

	// 	if count1 >= 60 {
	// 		utils.Rdb.LPop(ctx_lpop1, fmt.Sprintf("computerlist:hour:%s", strIp)).Result()
	// 		utils.Rdb.RPush(ctx_rpush1, fmt.Sprintf("computerlist:hour:%s", strIp), timer)
	// 		utils.Rdb.Set(ctx_set1, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	// 	} else {
	// 		utils.Rdb.RPush(ctx_rpush1, fmt.Sprintf("computerlist:hour:%s", strIp), timer)
	// 		utils.Rdb.Set(ctx_set1, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	// 	}
	// 	wg.Done()
	// }()
	// ////redis存入数据，list保持一天
	// go func() {

	// 	if count2 >= 1440 {
	// 		utils.Rdb.LPop(ctx_lpop2, fmt.Sprintf("computerlist:day:%s", strIp)).Result()
	// 		utils.Rdb.RPush(ctx_rpush2, fmt.Sprintf("computerlist:day:%s", strIp), timer)
	// 		utils.Rdb.Set(ctx_set2, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	// 	} else {
	// 		utils.Rdb.RPush(ctx_rpush2, fmt.Sprintf("computerlist:day:%s", strIp), timer)
	// 		utils.Rdb.Set(ctx_set2, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	// 	}
	// 	wg.Done()
	// }()
	////redis存入数据，list保持七天

	if count >= 10080 {
		utils.Rdb.LPop(ctx_lpop, fmt.Sprintf("computerlist:%s", strIp)).Result()
		utils.Rdb.RPush(ctx_rpush, fmt.Sprintf("computerlist:%s", strIp), js)
		// utils.Rdb.Set(ctx_set3, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	} else {
		utils.Rdb.RPush(ctx_rpush, fmt.Sprintf("computerlist:%s", strIp), js)
		// utils.Rdb.Set(ctx_set3, fmt.Sprintf("computer:%s:%s", strIp, strconv.Itoa(int(timer))), string(js), time.Hour*24*8)
	}

}

type SCpuData struct {
	CpuUsed string `json:"cpuUsed" db:"cpuUsed"`
}
type SMemData struct {
	MemTotal     string `json:"memTotal" db:"memTotal"`
	MemAvailable string `json:"memAvailable" db:"memAvailable"`
}
type SDiskData struct {
	DiskTotal string `json:"diskTotal" db:"diskTotal"`
	DiskUsed  string `json:"diskUsed" db:"diskUsed"`
}
type ST struct {
	Date string `json:"date" db:"date"`
	Time string `json:"time" db:"time"`
}
type SData struct {
	DateTime ST        `json:"dateTime" db:"dateTime"`
	Cpu      SCpuData  `json:"cpu" db:"cpu"`
	Mem      SMemData  `json:"mem" db:"mem"`
	Disk     SDiskData `json:"disk" db:"disk"`
}
