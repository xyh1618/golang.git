package utils

import (
	"errors"
	"fmt"
	"os"
)

// var FileName = os.Args[1]
type FileInfo struct {
	Name string `json:"name"`
	Data string `json:"date"`
}

func ReadDir(path string) ([]FileInfo, error) {
	list := make([]FileInfo, 0)
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	info, err := file.Stat()
	if err != nil {
		fmt.Println(err)
	}
	if !info.IsDir() {
		return nil, errors.New("wring")
	}
	d, err := file.ReadDir(-1)
	if err != nil {
		fmt.Println(err)
	}
	for _, name := range d {
		fileInfo, err := name.Info()
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		a := fileInfo.ModTime().Format("2006-01-02 15:04:05")

		list = append(list, FileInfo{Name: name.Name(), Data: a})
	}
	return list, nil
}
