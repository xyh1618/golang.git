package utils

import (
	"agent/config"
	"context"

	"github.com/go-redis/redis/v8"
)

var Rdb = new(redis.Client)

var opt = &redis.Options{
	Addr:     config.Profile().Redis_addr,
	Password: "SUGAR-blockchain@#321",
	DB:       100,
}

func init() {
	Rdb = redis.NewClient(opt)

	_, err := Rdb.Ping(context.Background()).Result()
	if err != nil {
		panic(err)
	}
}
