package controller

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
)

func DeleteFile(c *gin.Context) {
	out := new(DeletePathAndFilename)
	data := c.Param("value")
	newDate, _ := hex.DecodeString(data)
	err := json.Unmarshal(newDate, out)
	if err != nil {
		fmt.Println(err, 1111)
		c.JSON(500, gin.H{"code": 500, "msg": "参数错误"})
	}
	err = os.Remove(fmt.Sprintf("%s/%s", out.Path, out.FileName))
	if err != nil {
		fmt.Println(err, 1111)
		c.JSON(400, gin.H{"code": 400, "msg": "文件错误"})
	}
	c.JSON(200, gin.H{"code": 200, "msg": "ok"})
}

type DeletePathAndFilename struct {
	Path     string `json:"path"`
	FileName string `json:"fileName"`
}
