package controller

import (
	"agent/utils"
	"context"
	"encoding/hex"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/hpcloud/tail"
)

// 建立通道
var message = make(chan string)

// 建立长链接配置文件
var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// 日志收集工具
var config = tail.Config{
	ReOpen:    true,
	MustExist: false,
	Poll:      true,
	Follow:    true,
	Location:  &tail.SeekInfo{Offset: 0, Whence: 2},
}

func LogServer(c *gin.Context) {
	out := c.Param("value")
	pathName, err := hex.DecodeString(out)
	fmt.Println(string(pathName), 1111111111)
	//设置取消程序
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	//升级长链接
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	utils.Checkout(err)
	defer ws.Close()
	//循环接收发送数据
	for {
		mt, msg, err := ws.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println(string(msg), 444444444)
		if string(msg) == "stop" {
			cancel()
			err = ws.WriteMessage(mt, []byte("stop .... ok"))
			fmt.Println("结束", 22222)
			ws.Close()
			break

		} else if string(msg) == "start" {

			go msgWrite(ctx, ws, string(pathName))

		} else {

			continue
		}

	}
}

// 扫描文件数据
func msgWrite(ctx context.Context, ws *websocket.Conn, name string) {
	// ticker := time.NewTicker(time.Second * 2)
	// defer ticker.Stop()
	tails, err := tail.TailFile(name, config)
	if err != nil {

		fmt.Println(err)
	}

	for {
		timer := time.NewTimer(time.Second * 2)
		time.Sleep(time.Millisecond * 500)
		select {
		case <-ctx.Done():
			timer.Stop()
			tails.Stop()    //停止文件读取服务
			tails.Cleanup() //停止服务器系统对文件的监控服务

			return
		case line, ok := <-tails.Lines:

			if !ok {
				fmt.Println("tail file close")

				continue
			}
			err = ws.WriteMessage(websocket.TextMessage, []byte(line.Text))
			if err != nil {
				fmt.Println("是不是这里有问题", err)

				return
			}
			fmt.Println(line.Text, 3333333)
			timer.Stop()
		case <-timer.C:

			err = ws.WriteMessage(websocket.TextMessage, []byte("没有新日志"))
			if err != nil {
				fmt.Println("是不是这里有问题", err)

				return
			}
			fmt.Println(3333333)
		}

	}
}
