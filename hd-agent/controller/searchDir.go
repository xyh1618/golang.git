package controller

import (
	"agent/utils"
	"encoding/hex"

	"github.com/gin-gonic/gin"
)

func SearchDir(c *gin.Context) {
	data := c.Param("value")
	newDate, _ := hex.DecodeString(data)
	out, err := utils.ReadDir(string(newDate))
	if err != nil {
		c.JSON(500, gin.H{"code": 501, "msg": "验证签名失败"})
	}
	c.JSON(200, gin.H{"code": 201, "msg": "ok", "data": out})

}
