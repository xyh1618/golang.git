package main

import (
	"agent/router"
	"agent/utils"

	"time"
)

func main() {

	//程序结束关闭服务
	defer func() {
		//关闭redis服务
		utils.Rdb.Close()

	}()
	//开启定时服务器数据收集任务
	go func() {
		// ticker := time.NewTicker(time.Minute * 1)
		ticker := time.NewTicker(time.Minute * 1)
		for range ticker.C {
			router.ComputerData()
		}
	}()

	//开启模块服务
	router.App()
}
