package config

import (
	"flag"
)

var Env string

func init() {
	flag.StringVar(&Env, "env", "test", "默认为test,有效选项为test,prod")
}
func Profile() *EnvFile {
	envFile := new(EnvFile)

	flag.Parse()
	if Env == "test" {
		envFile.Redis_addr = "bj-crs-k4n91pc1.sql.tencentcdb.com:20035"

	} else if Env == "prod" {
		envFile.Redis_addr = "redis.huandianjifen.com:6379"

	} else {
		panic("环境变量输入错误")
	}
	return envFile
}

type EnvFile struct {
	Redis_addr string
}
