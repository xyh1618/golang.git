package main

import (
	blc "bitcoin/BLC"
	"math/big"

	"fmt"
)

func main() {
	blockchain := blc.CreateBlockchainIncludeGenesisBlock()
	fmt.Printf("newblock:%#v\n", blockchain)
	fmt.Println(blockchain.Blocks[0])
	blockchain.AddBlockToBlockchain("send 10RMB to zhangsan")
	blockchain.AddBlockToBlockchain("send 10RMB to lisi	")
	blockchain.AddBlockToBlockchain("send 10RMB to wangwu")
	fmt.Println(blockchain)
	aa := new(big.Int)
	aa.Lsh(big.NewInt(5), 2)
	fmt.Println(aa)
}
