package blc

import (
	"bytes"
	"encoding/binary"
)

func Int64ToBinaryToBytes(n int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, n)
	if err != nil {
		panic(err)
	}
	return buff.Bytes()
}
