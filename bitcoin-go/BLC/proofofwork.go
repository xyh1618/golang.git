package blc

import "math/big"

const targetBits = 16

type ProofOfWork struct {
	Block  *Block
	Target *big.Int
}

func NewProofOfWork(block *Block) *ProofOfWork {

	target := big.NewInt(1)
	target = target.Lsh(target, 256-targetBits)
	return &ProofOfWork{Block: block, Target: target}
}

func (pow *ProofOfWork) Run() ([]byte, int64) {

	return nil, 0
}
