package blc

import (
	"time"
)

type Block struct {
	//区块高度
	High int64
	//前一个区块的哈希
	PreBlockHash []byte
	//数据交易内容
	Data []byte
	//时间戳
	TimeStamp int64
	//当前区块哈希
	CurHash []byte
	//添加随机数
	Nonce int64
}

//	func (block *Block) SetCurHash() {
//		//heigh类型转换为bytes
//		heighbytes := Int64ToBinaryToBytes(block.High)
//		//timestamp转换为bytes
//		timeString := strconv.FormatInt(block.TimeStamp, 2)
//		timebytes := []byte(timeString)
//		//拼接数据
//		blockbytes := bytes.Join([][]byte{heighbytes, block.PreBlockHash, block.Data, timebytes, block.CurHash}, []byte{})
//		hash := sha256.Sum256(blockbytes)
//		block.CurHash = hash[:]
//	}

func NewBlock(data string, high int64, preBlockHash []byte) *Block {
	block := &Block{
		TimeStamp:    time.Now().Unix(),
		High:         high,
		PreBlockHash: preBlockHash,
		Data:         []byte(data),
		CurHash:      nil,
		Nonce:        0,
	}
	//工作量证明
	proofofwork := NewProofOfWork(block)
	hash, nonce := proofofwork.Run()
	block.CurHash = hash[:]
	block.Nonce = nonce
	return block
}

// 单独写一个方法生成创世区块
func CreateGenesisBlock(data string) *Block {
	return NewBlock(data, 1, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
}
