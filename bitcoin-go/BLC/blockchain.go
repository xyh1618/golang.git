package blc

type Blockchain struct {
	Blocks []*Block
}

func CreateBlockchainIncludeGenesisBlock() *Blockchain {
	genesisBlock := CreateGenesisBlock("Genesis Data ........")
	return &Blockchain{[]*Block{genesisBlock}}
}

func (blc *Blockchain) AddBlockToBlockchain(data string) {
	newBlock := NewBlock(data, blc.Blocks[len(blc.Blocks)-1].High, blc.Blocks[len(blc.Blocks)-1].CurHash)
	blc.Blocks = append(blc.Blocks, newBlock)
}
